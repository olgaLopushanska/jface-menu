package view;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;

import comparator.StudentViewerComparator;
import listeners.LoadFromFileMenuItemSelectionListener;
import listeners.SaveToFileMenuItemSelectionListener;
import model.Person;

/**
 * This view class creates application window, crates table, menu, form for user input.
 */
public class StudentView extends ApplicationWindow {

    private Composite parent;
    private Text nameText;
    private Text groupText;
    private Button checkBoxIsDone;
    private Button newButton;
    private Button saveButton;
    private Button deleteButton;
    private Button cancelButton;
    private TableViewer viewer;
    private Label errorLabel;
    private HashMap<String, String> errorMap;
    private Composite compositeBorderForNameText;
    private Composite compositeBorderForGroupText;
    private MenuItem newItem;
    private MenuItem cancelItem;
    private MenuItem saveItem;
    private MenuItem deleteItem;
    private MenuItem loadFromFileItem;
    private MenuItem saveToFileItem;

    public StudentView() {
        super(null);
        create();
        errorMap = new HashMap<>();

    }

    @Override
    protected Control createContents(Composite parent) {
        this.parent = parent;
        init();
        parent.pack();
        return parent;
    }

    public void handleShellEvents() {
        setBlockOnOpen(true);
        open();
        Display.getCurrent().dispose();
    }

    private void init() {
        parent.getShell().setText("JFace Student");
        createMenu();
        SashForm sashForm = new SashForm(parent, SWT.HORIZONTAL);
        ImageRegistry imageRegistry = createImageRegistry();
        createTable(sashForm, imageRegistry);
        createForm(sashForm);
    }

    private void createMenu() {
        Menu menu = new Menu(parent.getShell(), SWT.BAR);
        MenuItem fileItem = new MenuItem(menu, SWT.CASCADE);
        fileItem.setText("File");
        MenuItem editItem = new MenuItem(menu, SWT.CASCADE);
        editItem.setText("Edit");
        MenuItem helpItem = new MenuItem(menu, SWT.PUSH);
        helpItem.setText("Help");
        parent.getShell().setMenuBar(menu);
        addListenerToHelpMenuItem(helpItem);
        Menu editSubMenu = new Menu(menu);
        editItem.setMenu(editSubMenu);
        newItem = new MenuItem(editSubMenu, SWT.PUSH);
        newItem.setText("New");
        saveItem = new MenuItem(editSubMenu, SWT.PUSH);
        saveItem.setText("Save");
        saveItem.setEnabled(false);
        deleteItem = new MenuItem(editSubMenu, SWT.PUSH);
        deleteItem.setText("Delete");
        deleteItem.setEnabled(false);
        cancelItem = new MenuItem(editSubMenu, SWT.PUSH);
        cancelItem.setText("Cancel");

        Menu fileSubMenu = new Menu(menu);
        fileItem.setMenu(fileSubMenu);
        loadFromFileItem = new MenuItem(fileSubMenu, SWT.PUSH);
        loadFromFileItem.setText("Load from file");
        saveToFileItem = new MenuItem(fileSubMenu, SWT.PUSH);
        saveToFileItem.setText("Save to file");
    }

    private void addListenerToHelpMenuItem(MenuItem helpItem) {
        helpItem.addSelectionListener(new SelectionAdapter() {
            String str = "This application is made for saving students entries.\n"
                    + "User can create entry using the form and buttons New or Save.\n"
                    + "User can change already made entry by choosing it from table and "
                    + "modifying it fields and with button Save\n" + "User can delete entry with appropriate button\n"
                    + "All these actions can be done with menu also\n"
                    + "User can upload entries from file and save entries ito file";

            @Override
            public void widgetSelected(SelectionEvent selectionEvent) {
                MessageDialog dialog = new MessageDialog(parent.getShell(), "", null, str, MessageDialog.NONE,
                        new String[] {}, 0);
                dialog.open();
            }
        });
    }

    private void createTable(SashForm sashForm, ImageRegistry imageRegistry) {
        Composite tableComposite = new Composite(sashForm, SWT.NONE);
        TableColumnLayout tableColumnLayout = new TableColumnLayout();
        tableComposite.setLayout(tableColumnLayout);
        viewer = new TableViewer(tableComposite, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

        TableViewerColumn colName = new TableViewerColumn(viewer, SWT.NONE);
        colName.getColumn().setText("Name");
        tableColumnLayout.setColumnData(colName.getColumn(), new ColumnWeightData(20, 200, true));
        colName.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object el) {
                Person p = (Person) el;
                return p.getName();
            }
        });
        StudentViewerComparator comparatorForColName = new StudentViewerComparator(viewer, colName) {

            @Override
            public int doCompare(Viewer viewer, Object o1, Object o2) {
                Person p1 = (Person) o1;
                Person p2 = (Person) o2;
                return p1.getName().compareTo(p2.getName());
            }
        };

        TableViewerColumn colGroup = new TableViewerColumn(viewer, SWT.NONE);
        colGroup.getColumn().setText("Group");
        tableColumnLayout.setColumnData(colGroup.getColumn(), new ColumnWeightData(20, 200, true));
        colGroup.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object el) {
                Person p = (Person) el;
                return String.valueOf(p.getGroup());
            }
        });

        new StudentViewerComparator(viewer, colGroup) {

            @Override
            public int doCompare(Viewer viewer, Object o1, Object o2) {
                Person p1 = (Person) o1;
                Person p2 = (Person) o2;
                return p1.getGroup() - p2.getGroup();
            }
        };

        TableViewerColumn colSWTDone = new TableViewerColumn(viewer, SWT.NONE);
        colSWTDone.getColumn().setText("SWT done");
        tableColumnLayout.setColumnData(colSWTDone.getColumn(), new ColumnWeightData(20, 200, true));
        addLabelProviderToIsDoneColumn(colSWTDone, imageRegistry);
        viewer.getTable().setLinesVisible(true);
        viewer.getTable().setHeaderVisible(true);
        viewer.setContentProvider(ArrayContentProvider.getInstance());
        comparatorForColName.setSorter(comparatorForColName, StudentViewerComparator.ASC);
        addListenerToViewer();
    }

    private void addLabelProviderToIsDoneColumn(TableViewerColumn colSWTDone, ImageRegistry imageRegistry) {
        colSWTDone.setLabelProvider(new ColumnLabelProvider() {
            @Override
            public String getText(Object el) {
                return null;
            }

            @Override
            public Image getImage(Object element) {
                if (((Person) element).isDone()) {
                    return imageRegistry.get("checked");
                } else {
                    return imageRegistry.get("unchecked");
                }
            }
        });
    }

    private void addListenerToViewer() {
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {

            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                IStructuredSelection selection = getTableSelection();
                Object element = selection.getFirstElement();
                if (element == null) {
                    enableButtons(false);
                    enableMenuItems(false);
                    return;
                }
                enableButtons(true);
                enableMenuItems(true);
                setFields(((Person) element).getName(), String.valueOf(((Person) element).getGroup()),
                        ((Person) element).isDone());
                clearError();
            }
        });
    }

    private ImageRegistry createImageRegistry() {
        ImageRegistry imageRegistry = new ImageRegistry();
        Image checkedImage = null;
        Image uncheckedImage = null;

        try (InputStream checkedStream = new FileInputStream("images/checked.gif")) {
            ImageData checkedImageData = new ImageData(checkedStream);
            checkedImage = new Image(parent.getDisplay(), checkedImageData);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        try (InputStream uncheckedStream = new FileInputStream("images/unchecked.gif")) {
            ImageData uncheckedImageData = new ImageData(uncheckedStream);
            uncheckedImage = new Image(parent.getDisplay(), uncheckedImageData);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        imageRegistry.put("checked", checkedImage);
        imageRegistry.put("unchecked", uncheckedImage);
        return imageRegistry;
    }

    private void createForm(SashForm sashForm) {
        Composite form = new Composite(sashForm, SWT.NONE);
        form.setLayout(new GridLayout(2, true));
        form.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        createPanelForNameGroupCheckBox(form);
        createButtonPanel(form);
    }

    private void createPanelForNameGroupCheckBox(Composite form) {
        Composite panelForNameGroupCheckBox = new Composite(form, SWT.NONE);
        GridLayout gridLayoutForPanel = new GridLayout(2, true);
        gridLayoutForPanel.marginLeft = 25;
        gridLayoutForPanel.marginRight = 25;
        panelForNameGroupCheckBox.setLayout(gridLayoutForPanel);
        GridData gridDataForPanel = new GridData(SWT.FILL, SWT.NONE, true, false);
        gridDataForPanel.horizontalSpan = 2;
        panelForNameGroupCheckBox.setLayoutData(gridDataForPanel);
        createNameAndGroupPanel(panelForNameGroupCheckBox);
        Label isDoneLabel = new Label(panelForNameGroupCheckBox, SWT.NONE);
        isDoneLabel.setText("SWT task done");
        checkBoxIsDone = new Button(panelForNameGroupCheckBox, SWT.CHECK);
        GridData gridDataForCheckBox = new GridData(SWT.END, SWT.NONE, false, false);
        checkBoxIsDone.setLayoutData(gridDataForCheckBox);
        errorLabel = new Label(panelForNameGroupCheckBox, SWT.WRAP);
        errorLabel.setLayoutData(gridDataForPanel);
    }

    private void createNameAndGroupPanel(Composite form) {
        Composite namePanel = new Composite(form, SWT.NONE);
        GridLayout gridLayoutForPanel = new GridLayout(2, false);
        gridLayoutForPanel.marginWidth = 0;
        GridData gridDataForPanel = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridDataForPanel.horizontalSpan = 2;
        namePanel.setLayout(gridLayoutForPanel);
        namePanel.setLayoutData(gridDataForPanel);
        Composite groupPanel = new Composite(form, SWT.NONE);
        groupPanel.setLayout(gridLayoutForPanel);
        groupPanel.setLayoutData(gridDataForPanel);

        Label nameLabel = new Label(namePanel, SWT.NONE);
        nameLabel.setText("Name");
        createTextFieldWithBorder("name", namePanel);
        Label groupLabel = new Label(groupPanel, SWT.NONE);
        groupLabel.setText("Group");
        createTextFieldWithBorder("group", groupPanel);
    }

    private void createTextFieldWithBorder(String fieldName, Composite groupPanel) {
        Composite composite = new Composite(groupPanel, SWT.NONE);
        Text text = new Text(composite, SWT.SINGLE | SWT.BORDER);

        if (fieldName.equals("name")) {
            compositeBorderForNameText = composite;
            nameText = text;
            addListenerToNameText();
        } else {
            compositeBorderForGroupText = composite;
            groupText = text;
            addListenerToGroupText();
        }
        // make red color border round the group text field
        GridData gridDataForField = new GridData(SWT.FILL, SWT.NONE, true, false);
        composite.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));
        composite.setLayoutData(gridDataForField);
        FillLayout layout = new FillLayout();
        layout.marginHeight = layout.marginWidth = 1;
        composite.setLayout(layout);
        text.setLayoutData(gridDataForField);
    }

    private void addListenerToGroupText() {
        groupText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                e.doit = false;
                if (Character.isDigit(e.character)) {
                    e.doit = true;
                    clearError("group");
                }
                checkForBSAndArrowButtons(e);
            }

        });
    }

    private void addListenerToNameText() {
        nameText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                e.doit = false;
                if (Character.isLetter(e.character)) {
                    e.doit = true;
                    clearError("name");
                }
                checkForBSAndArrowButtons(e);
            }
        });
    }

    private void checkForBSAndArrowButtons(KeyEvent e) {
        if (e.keyCode == SWT.BS) {
            e.doit = true;
        }
        if (e.keyCode == SWT.ARROW_LEFT) {
            e.doit = true;
        }
        if (e.keyCode == SWT.ARROW_RIGHT) {
            e.doit = true;
        }
    }

    private void createButtonPanel(Composite form) {
        Composite compositeForButtons = new Composite(form, SWT.NONE);
        GridLayout gridLayoutForPanel = new GridLayout(4, true);
        gridLayoutForPanel.marginWidth = 0;
        gridLayoutForPanel.horizontalSpacing = 10;
        compositeForButtons.setLayout(gridLayoutForPanel);
        GridData gridDataForButtonPanel = new GridData(SWT.FILL, SWT.END, true, true);
        gridDataForButtonPanel.horizontalSpan = 2;
        compositeForButtons.setLayoutData(gridDataForButtonPanel);

        newButton = new Button(compositeForButtons, SWT.PUSH);
        newButton.setText("New");
        GridData dataForBut = new GridData(SWT.FILL, SWT.NONE, true, true);
        newButton.setLayoutData(dataForBut);
        saveButton = new Button(compositeForButtons, SWT.PUSH);
        saveButton.setText("Save");
        saveButton.setLayoutData(dataForBut);
        saveButton.setEnabled(false);
        deleteButton = new Button(compositeForButtons, SWT.PUSH);
        deleteButton.setText("Delete");
        deleteButton.setLayoutData(dataForBut);
        deleteButton.setEnabled(false);
        cancelButton = new Button(compositeForButtons, SWT.PUSH);
        cancelButton.setText("Cancel");
        cancelButton.setLayoutData(dataForBut);
    }

    public String getNameText() {
        return nameText.getText();
    }

    public void setNameForTextField(String name) {
        nameText.setText(name);
    }

    public String getGroupText() {
        return groupText.getText();
    }

    public void setGroupForTextField(String group) {
        groupText.setText(group);
    }

    public String getCheckBoxIsDone() {
        return String.valueOf(checkBoxIsDone.getSelection());
    }

    public void setCheckBoxIsDone(String text) {
        checkBoxIsDone.setText(text);
    }

    public void addListenerToNewButton(SelectionListener listener) {
        newButton.addSelectionListener(listener);
    }

    public void updateDataForTable(List<Person> personList) {
        viewer.setInput(personList);
    }

    /**
     * method adds values to error map
     * 
     * @param key
     * @param value
     */
    public void setTextForErrorLabel(String key, String value) {
        errorMap.put(key, value);
        if (key.equals("name")) {
            compositeBorderForNameText.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_RED));
        }
        if (key.equals("group")) {
            compositeBorderForGroupText.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_RED));
        }
        showError();
    }

    /**
     * method clears error according to key
     * 
     * @param key
     */
    public void clearError(String key) {
        errorMap.put(key, "");
        if (key.equals("name")) {
            compositeBorderForNameText.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));
        }
        if (key.equals("group")) {
            compositeBorderForGroupText.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));
        }
        showError();
    }

    private void showError() {
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, String> entry : errorMap.entrySet()) {
            str.append(entry.getValue());
        }
        errorLabel.setText(str.toString());
    }

    public void setSelectionForCheckBox(boolean selection) {
        checkBoxIsDone.setSelection(selection);
    }

    public IStructuredSelection getTableSelection() {
        return viewer.getStructuredSelection();
    }

    public void addSelectionListenerToSaveButton(SelectionListener listener) {
        saveButton.addSelectionListener(listener);
    }

    public void enableButtons(boolean state) {
        saveButton.setEnabled(state);
        deleteButton.setEnabled(state);
    }

    public void enableMenuItems(boolean state) {
        saveItem.setEnabled(state);
        deleteItem.setEnabled(state);
    }

    public void addSelectionListenerToDeleteButton(SelectionListener listener) {
        deleteButton.addSelectionListener(listener);
    }

    public void addSelectionListenerToCancelButton(SelectionListener listener) {
        cancelButton.addSelectionListener(listener);
    }

    public void setTableSelectionToNone() {
        viewer.setSelection(null);
    }

    public Composite getParent() {
        return parent;
    }

    public void addSelectionListenerToNewMenuItem(SelectionListener listener) {
        newItem.addSelectionListener(listener);
    }

    public void addSelectionListenerToCancelMenuItem(SelectionListener listener) {
        cancelItem.addSelectionListener(listener);
    }

    public void addSelectionListenerToDeleteMenuItem(SelectionListener listener) {
        deleteItem.addSelectionListener(listener);
    }

    public void addSelectionListenerToSaveMenuItem(SelectionListener listener) {
        saveItem.addSelectionListener(listener);
    }

    public void clearError() {
        clearError("name");
        clearError("group");
    }

    public void setFields(String name, String group, boolean isDone) {
        setNameForTextField(name);
        setGroupForTextField(group);
        setSelectionForCheckBox(isDone);
    }

    public void addSelectionListenerToLoadFromFileMenuItem(LoadFromFileMenuItemSelectionListener listener) {
        loadFromFileItem.addSelectionListener(listener);
    }

    public void addSelectionListenerToSaveToFileMenuItem(SaveToFileMenuItemSelectionListener listener) {
        saveToFileItem.addSelectionListener(listener);
    }
}
