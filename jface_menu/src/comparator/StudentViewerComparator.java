package comparator;

import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;

public abstract class StudentViewerComparator extends ViewerComparator {
    private static int sortingDirection = 1;

    public static final int ASC = 1;
    public static final int DESC = -1;

    private TableViewerColumn column;
    private ColumnViewer viewer;

    public StudentViewerComparator(ColumnViewer viewer, TableViewerColumn column) {
        this.column = column;
        this.viewer = viewer;
        ColumnSelectionListener selectionAdapter = new ColumnSelectionListener(this);
        this.column.getColumn().addSelectionListener(selectionAdapter);
    }

    public void setSorter(StudentViewerComparator sorter, int direction) {
        Table columnParent = column.getColumn().getParent();

        columnParent.setSortColumn(column.getColumn());
        sortingDirection = direction;
        columnParent.setSortDirection(direction == ASC ? SWT.DOWN : SWT.UP);

        if (viewer.getComparator() == sorter) {
            viewer.refresh();
        } else {
            viewer.setComparator(sorter);
        }
    }

    @Override
    public int compare(Viewer viewer, Object e1, Object e2) {
        return sortingDirection * doCompare(viewer, e1, e2);
    }

    protected abstract int doCompare(Viewer viewer, Object e1, Object e2);

    public static int getSortingDirection() {
        return sortingDirection;
    }

    public ColumnViewer getViewer() {
        return viewer;
    }
}
