package comparator;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class ColumnSelectionListener extends SelectionAdapter {
    public static final int ASC = 1;
    public static final int DESC = -1;

    private StudentViewerComparator comparator;

    public ColumnSelectionListener(StudentViewerComparator comparator) {
        this.comparator = comparator;
    }

    @Override
    public void widgetSelected(SelectionEvent e) {

        int sortingDirection = StudentViewerComparator.getSortingDirection();
        if (sortingDirection == ASC) {
            comparator.setSorter(comparator, DESC);
        } else {
            comparator.setSorter(comparator, ASC);
        }
    }
}
