package listeners;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import controller.StudentController;
import model.StudentModel;
import model.Person;
import view.StudentView;

public class SaveButtonSelectionListener extends SelectionAdapter {
    private StudentController controller;
    private StudentView view;
    private StudentModel model;

    public SaveButtonSelectionListener(StudentController controller) {
        this.controller = controller;
        this.view = controller.getView();
        this.model = controller.getModel();
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        IStructuredSelection selection = view.getTableSelection();
        Object oldPerson = selection.getFirstElement();
        if (oldPerson == null) {
            return;
        }
        Person newPerson = controller.validateFieldsAndCreatePerson();
        if (newPerson == null) {
            return;
        }
        model.updatePerson((Person) oldPerson, newPerson);
        view.setFields("", "", false);
        view.clearError();
        view.setTableSelectionToNone();
        controller.updatePersonDataInTable();
    }
}
