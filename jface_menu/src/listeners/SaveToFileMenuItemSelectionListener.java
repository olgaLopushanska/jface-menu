package listeners;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import controller.StudentController;
import model.Person;

public class SaveToFileMenuItemSelectionListener extends SelectionAdapter {
    private StudentController controller;

    public SaveToFileMenuItemSelectionListener(StudentController controller) {
        this.controller = controller;
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        FileDialog dlg = new FileDialog((Shell) controller.getView().getParent(), SWT.MULTI);
        dlg.setFilterExtensions(new String[] { "*.csv" });
        dlg.open();
        String fileName = dlg.getFilterPath() + "\\" + dlg.getFileName();
        String str = createStringOfPersonData();
        if (str.equals("")) {
            showError("There is no personal data at the table");
            return;
        }
        try (FileWriter myWriter = new FileWriter(fileName)) {
            myWriter.write(str);
            myWriter.close();
        } catch (IOException e) {
            showError(e.getMessage());
            System.out.println(e.getMessage());
        }
    }

    private String createStringOfPersonData() {
        List<Person> list = controller.getModel().getPersonList();
        if (list == null || list.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Person p : list) {
            String s = p.getName() + "," + String.valueOf(p.getGroup()) + "," + String.valueOf(p.isDone());
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    private void showError(String str) {
        MessageDialog dialog = new MessageDialog((Shell) (controller.getView().getParent()), "", null, str,
                MessageDialog.NONE, new String[] {}, 0);
        dialog.open();
    }
}
