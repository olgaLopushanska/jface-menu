package listeners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import controller.StudentController;
import model.Person;

public class LoadFromFileMenuItemSelectionListener extends SelectionAdapter {
    private StudentController controller;

    public LoadFromFileMenuItemSelectionListener(StudentController controller) {
        this.controller = controller;
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        FileDialog dlg = new FileDialog((Shell) (controller.getView().getParent()), SWT.NONE);
        dlg.setFilterExtensions(new String[] { "*.csv" });
        dlg.open();
        String fileName = dlg.getFilterPath() + "\\" + dlg.getFileName();
        File file = new File(fileName);
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file));) {
            String st;
            while ((st = br.readLine()) != null) {
                sb.append(st);
                if (br != null) {
                    sb.append("\n");
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            showError(e.getMessage());
            return;
        }
        if (sb == null || sb.length() == 0) {
            showError("File is empty");
            return;
        }
        saveInModel(sb.toString());
    }

    private void saveInModel(String str) {
        String[] array = str.split("\n");
        if (array == null) {
            showError("File contains invalid information");
            return;
        }
        List<Person> list = new ArrayList<>();
        for (String s : array) {
            if (s == null || s.equals("")) {
                showError("File contains invalid information");
                return;
            }
            String arrayOfPersonValues[] = s.split(",");
            if (arrayOfPersonValues == null || arrayOfPersonValues.length != 3) {
                showError("File contains invalid information");
                return;
            }
            for (String some : arrayOfPersonValues) {
                if (some.equals("")) {
                    showError("File contains invalid information");
                    return;
                }
            }
            if (!checkForValidName(arrayOfPersonValues[0])) {
                showError("File contains invalid information");
                return;
            }
            int group = 0;
            boolean isDone = false;
            try {
                group = Integer.valueOf(arrayOfPersonValues[1]);
                isDone = Boolean.valueOf(arrayOfPersonValues[2]);
            } catch (Exception e) {
                showError("File contains invalid information");
                System.out.println(e.getMessage());
                return;
            }
            Person person = new Person(arrayOfPersonValues[0], group, isDone);
            list.add(person);
        }
        controller.getModel().readFromFile(list);
        controller.updatePersonDataInTable();
    }

    private void showError(String str) {
        MessageDialog dialog = new MessageDialog((Shell) (controller.getView().getParent()), "", null, str,
                MessageDialog.NONE, new String[] {}, 0);
        dialog.open();
    }

    private boolean checkForValidName(String str) {
        boolean isValidName = true;
        char[] name = str.toCharArray();
        for (int counter = 0; counter < name.length; counter++) {
            if (!Character.isLetter(name[counter])) {
                return false;
            }
        }
        return isValidName;
    }
}
