package listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import controller.StudentController;
import model.StudentModel;
import model.Person;
import view.StudentView;

public class NewButtonSelectionListener extends SelectionAdapter {
    private StudentView view;
    private StudentModel model;
    private StudentController controller;

    public NewButtonSelectionListener(StudentController controller) {
        this.controller = controller;
        this.view = controller.getView();
        this.model = controller.getModel();
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        Person person = controller.validateFieldsAndCreatePerson();
        if (person == null) {
            return;
        }
        model.addEntryToList(person);
        controller.updatePersonDataInTable();
        view.setFields("", "", false);
        view.clearError();
    }
}
