package listeners;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import controller.StudentController;
import model.StudentModel;
import model.Person;
import view.StudentView;

public class DeleteButtonSelectionListener extends SelectionAdapter {
    private StudentView view;
    private StudentModel model;
    private StudentController controller;

    public DeleteButtonSelectionListener(StudentController controller) {
        this.view = controller.getView();
        this.model = controller.getModel();
        this.controller = controller;
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        IStructuredSelection selection = view.getTableSelection();
        Object person = selection.getFirstElement();
        if (person == null) {
            return;
        }
        model.deletePerson((Person) person);
        controller.updatePersonDataInTable();
        view.enableButtons(false);
        view.enableMenuItems(false);
        view.setFields("", "", false);
        view.clearError();
    }
}
