package listeners;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Shell;

import controller.StudentController;
import model.Person;
import view.StudentView;

public class CancelButtonSelectionListener extends SelectionAdapter {

    private StudentController controller;
    private StudentView view;

    public CancelButtonSelectionListener(StudentController controller) {
        this.controller = controller;
        this.view = controller.getView();
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        MessageDialog dialog = new MessageDialog((Shell) view.getParent(), "", null, "Wanna save your changes?",
                MessageDialog.NONE, new String[] { "Yes", "No" }, 0);
        int result = dialog.open();
        IStructuredSelection selection = view.getTableSelection();
        Object oldPerson = selection.getFirstElement();
        if (result == 0) {
            if (oldPerson == null) {
                new NewButtonSelectionListener(controller).widgetSelected(selectionEvent);
            } else {
                new SaveButtonSelectionListener(controller).widgetSelected(selectionEvent);
            }
        } else {
            if (oldPerson == null) {
                view.setFields("", "", false);
                view.setTableSelectionToNone();
                return;
            } else {
                view.setFields(((Person) oldPerson).getName(), String.valueOf(((Person) oldPerson).getGroup()),
                        ((Person) oldPerson).isDone());
            }
        }
    }
}
