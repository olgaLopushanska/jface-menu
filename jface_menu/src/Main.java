import controller.StudentController;
import model.StudentModel;
import view.StudentView;

public class Main {

    public static void main(String[] args) {
        StudentModel model = new StudentModel();
        StudentView menuView = new StudentView();
        StudentController controller = new StudentController(menuView, model);
        menuView.handleShellEvents();
    }
}
