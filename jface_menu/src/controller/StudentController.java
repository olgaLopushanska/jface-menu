package controller;

import view.StudentView;
import model.StudentModel;
import model.Person;
import listeners.NewButtonSelectionListener;
import listeners.SaveButtonSelectionListener;
import listeners.SaveToFileMenuItemSelectionListener;
import listeners.CancelButtonSelectionListener;
import listeners.DeleteButtonSelectionListener;
import listeners.LoadFromFileMenuItemSelectionListener;

import org.eclipse.swt.events.SelectionListener;

public class StudentController {
    private StudentView view;
    private StudentModel model;
 
    public StudentController(StudentView view, StudentModel model) {
        this.view = view;
        this.model = model;
        createAndAddListeners();
    }

    private void createAndAddListeners() {
        SelectionListener selectionListenerForNewButton = new NewButtonSelectionListener(this);
        SelectionListener selectionListenerForSaveButton = new SaveButtonSelectionListener(this);
        SelectionListener selectionListenerForDeleteButton = new DeleteButtonSelectionListener(this);
        SelectionListener selectionListenerForCancelButon = new CancelButtonSelectionListener(this);
        LoadFromFileMenuItemSelectionListener selectionListenerForLoadFromFile = new LoadFromFileMenuItemSelectionListener(this);
        SaveToFileMenuItemSelectionListener selectionListenerForSaveToFile = new SaveToFileMenuItemSelectionListener(this);
        view.addListenerToNewButton(selectionListenerForNewButton);
        view.addSelectionListenerToSaveButton(selectionListenerForSaveButton);
        view.addSelectionListenerToDeleteButton(selectionListenerForDeleteButton);
        view.addSelectionListenerToCancelButton(selectionListenerForCancelButon);
        view.addSelectionListenerToNewMenuItem(selectionListenerForNewButton);
        view.addSelectionListenerToCancelMenuItem(selectionListenerForCancelButon);
        view.addSelectionListenerToDeleteMenuItem(selectionListenerForDeleteButton);
        view.addSelectionListenerToSaveMenuItem(selectionListenerForSaveButton);
        view.addSelectionListenerToLoadFromFileMenuItem(selectionListenerForLoadFromFile);
        view.addSelectionListenerToSaveToFileMenuItem(selectionListenerForSaveToFile);
    }

    public StudentView getView() {
        return view;
    }

    public StudentModel getModel() {
        return model;
    }
    
    public Person validateFieldsAndCreatePerson() {
        boolean hasError = false;
        String name = view.getNameText().trim();
        if (name == null || name.equals("")) {
            view.setTextForErrorLabel("name", "Name must not be empty. Please enter only letters in field. ");
            hasError = true;
        }
        String group = view.getGroupText();
        int groupVal = 0;
        try {
            groupVal = Integer.parseInt(group);
            
        } catch (Exception e) {
            view.setTextForErrorLabel("group", "Group must not be empty. Please enter only digits in field. ");
            hasError = true;
        }
        if (hasError) {
            return null;
        }
        boolean isDone = Boolean.parseBoolean(view.getCheckBoxIsDone());
        Person person = new Person(name, groupVal, isDone);
        return person;
    }
    
    public void updatePersonDataInTable() {
        view.updateDataForTable(model.getPersonList());
    }
}
