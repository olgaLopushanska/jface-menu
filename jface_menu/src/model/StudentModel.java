package model;

import java.util.ArrayList;
import java.util.List;

/**
 * This model class stores all person entries and provides CRUD operations for it
 * 
 */
public class StudentModel {
    private List<Person> personList;

    public StudentModel() {
        personList = new ArrayList<Person>();
    }

    /**
     * method add new person entry to model
     */
    public void addEntryToList(Person p) {
        personList.add(p);
    }

    /**
     * method returns person data, that can not be modified
     */
    public List<Person> getPersonList() {
        return List.copyOf(personList);
    }

    /**
     * method update person entry according to valid user input
     */
    public void updatePerson(Person oldPerson, Person newPerson) {
        oldPerson.setName(newPerson.getName());
        oldPerson.setGroup(newPerson.getGroup());
        oldPerson.setDone(newPerson.isDone());
    }

    /**
     * method delete selected person entry from model
     */
    public void deletePerson(Person person) {
        personList.remove(person);
    }

    /**
     * replace person entries from model with those that were read from the file
     */
    public void readFromFile(List<Person> list) {
        personList = list;
    }
}
