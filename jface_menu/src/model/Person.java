package model;

import java.util.Objects;
import java.util.UUID;

/**
 * This person class stores all the information about the person such as: id, name, group and isDone field which changes
 * depending on the completion of the task.
 */
public class Person {
    private String name;
    private int group;
    private boolean isDone;
    private long id;

    public Person() {
        setID();
    }

    public Person(String name, int group, boolean isDone) {
        this();
        this.name = name;
        setGroup(group);
        this.isDone = isDone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group < 0 ? 0 : group;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean isDone) {
        this.isDone = isDone;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", group=" + group + ", isDone=" + isDone + ", id=" + id + "]";
    }

    private void setID() {
        this.id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, id, isDone, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        return group == other.group && id == other.id && isDone == other.isDone && Objects.equals(name, other.name);
    }
}
